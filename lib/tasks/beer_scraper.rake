desc "Get that beer"
task :beer => :environment do
  agent = Mechanize.new
  page = agent.get('http://www.mikesliquors.com/contents/en-us/d21.html')
  beers = page.search("h2.ProductTitle")
  beers.each do |beer|
    name = beer.children.text
    name = name.split(",")[0] if name
    find_beer = Beer.find_by_name(name)
    unless find_beer
      Beer.create(name: name)
    end
  end
end