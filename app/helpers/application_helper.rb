module ApplicationHelper

	def latest_review(user)
		unless user && user.reviews.empty?
			"#{user.reviews.last.beer.name} - #{user.reviews.last.rating}"	
		else
			"No reviews yet"
		end
	end
end
