class UsersController < ApplicationController
	# if Rails.env.production?
    # http_basic_authenticate_with :name => ENV['NAME'], :password => ENV['PING'], :except => [:show]
    # http_basic_authenticate_with :name => "test", :password => "test"
  # end
	before_filter :user, only: [:update, :edit]
	def index
		@user = User.find_by_id(session[:user], include: :reviews)
		@users = User.all.order(:name)
		@reviews = @user.reviews if @user
		@rated_beers = @reviews.map(&:beer).uniq if @reviews
		@ordered = Beer.ordered
		@beers = Beer.all.order("name asc")
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			redirect_to root_path
		else
			render action: :new
		end
	end

	def edit
	end

	def update
		if @user.update(user_params)
			redirect_to root_path
		else
			render action: :edit
		end
	end

	def set_user
		session[:user] = params[:user_id]
		redirect_to root_path
	end

	def rate
		Review.create(user_id: session[:user], rating: params[:rating], beer_id: params[:beer_id])
		redirect_to root_path
	end

	def ordered
		beer_id = params[:beer_id]
		beer = Beer.find(beer_id)
		beer.ordered = true
		beer.save
		redirect_to root_path
	end

	private

	def user
    @user = User.find(params[:id])
  end

	def user_params
    params.require(:user).permit(:name)
  end
end