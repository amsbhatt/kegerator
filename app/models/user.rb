class User < ActiveRecord::Base
	has_many :reviews, dependent: :destroy
	has_many :beers, through: :reviews
	validates :name, presence: true

	def average_review
		i = 0
		reviews.map{ |r| i += r.rating }
		i = i/(reviews.count) unless i == 0
		i
	end
end