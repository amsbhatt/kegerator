class Beer < ActiveRecord::Base
	has_many :reviews
	validates :name, presence: true

	scope :ordered, -> { where(ordered: true) } 

	def self.unordered
		Beer.all.map { |b| b if !b.ordered}.compact
	end

	def average_rating
		i = 0
		reviews.map { |r| i += r.rating }
		i = i/reviews.count unless i == 0
		i == 0 ? "-" : i
	end
end