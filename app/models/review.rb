class Review < ActiveRecord::Base
	belongs_to :user
	belongs_to :beer
	validates :user_id, :beer_id, :rating, presence: true

	RATING = (1..5).to_a.push("-")
end