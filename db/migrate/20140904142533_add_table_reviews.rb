class AddTableReviews < ActiveRecord::Migration
  def change
  	create_table :reviews do |t|
  		t.string :name
  		t.integer :rating
  		t.integer :user_id
      t.timestamps
  	end

  	add_index(:reviews, :user_id)
  end
end
