class AddOrderedToBeers < ActiveRecord::Migration
  def change
  	add_column :beers, :ordered, :boolean, default: false
  end
end
